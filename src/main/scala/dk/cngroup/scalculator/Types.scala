package dk.cngroup.scalculator

/**
 * Application custom types
 */
object Types {
  /** Library function */
  type Func = ((Int) => Int, Int) => (Int) => Int
  /** Definition of a function NAME,FUNC */
  type FuncDef = (String, Func)
  /** List of definitions of functions */
  type FuncList = List[FuncDef]
  /** Library of operations NAME -> FUNC */
  type FuncMap = Map[String, Func]
  /** Instruction OPERATION, OPERAND */
  type Instr = (String, Int)
  /** Iterator over instructions */
  type InstrIterator = Iterator[Instr]
}
