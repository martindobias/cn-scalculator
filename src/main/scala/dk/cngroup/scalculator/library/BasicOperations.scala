package dk.cngroup.scalculator.library

import dk.cngroup.scalculator.Types.{FuncList, Func}
import dk.cngroup.scalculator.OperationLibrary

/**
 * Library of very basic operations.
 */
object BasicOperations extends OperationLibrary {

  /** Sums a value to accumulator */
  val add: Func = (acc, v) => (x) => acc(x) + v
  /** Subtract value from accumulator */
  val subtract: Func = (acc, v) => (x) => acc(x) - v
  /** Multiply accumulator by value */
  val multiply: Func = (acc, v) => (x) => acc(x) * v
  /** Divide accumulator by value */
  val divide: Func = (acc, v) => (x) => acc(x) / v

  /**
   * Return operations mapping for a library
   * @return List of mappings NAME -> FUNCTION
   */
  override def mappings: FuncList =
    List(("ADD", add), ("SUB", subtract), ("MUL", multiply), ("DIV", divide))
}
