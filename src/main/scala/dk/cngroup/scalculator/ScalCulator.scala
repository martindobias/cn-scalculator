package dk.cngroup.scalculator

import dk.cngroup.scalculator.library.BasicOperations

/**
 * ScalCulator main object responsible for orchestrating the calculation.
 * Accepts command line arguments though inherited App interface.
 */
object ScalCulator extends App {
  val library = Library(BasicOperations)
  this.args.flatMap((file) => compute(Reader(file), library)).foreach((res) => println(res))
}
