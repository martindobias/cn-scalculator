package dk.cngroup.scalculator

import Types._

/**
 * Operation library interface. Each library can provide unlimited number of operations.
 * Library manager verifies there are no ambiguous definitions, operation name must be unique for ScalCulator to work.
 * Operation resolution is case dependent
 */
trait OperationLibrary {
  /**
   * Return operations mapping for a library
   * @return List of mappings NAME -> FUNCTION
   */
  def mappings: FuncList
}

/**
 * Thrown in case ambiguous definitions of operations found by Library manager.
 * @param operations list of names of ambiguous operations
 */
case class AmbiguousDefinitionsFoundException(operations: List[String])
  extends Exception(("Ambiguous definitions found for:\n" /: operations)((message: String, operation: String) => message + operation + "\n"))

/**
 * Thrown in case unrecognized operations requested.
 * @param operation unrecognized operation
 */
case class UnrecognizedOperationException(operation: String)
  extends Exception("Unrecognized operation requested: " + operation)

/**
 * Library manager interface
 */
trait Library {
  /**
   * Resolve operation name to a function
   * @param op Operation name
   * @return Operation function for calculation
   */
  def resolve(op: String): Func
}

/**
 * Default implementation of Library manager. A facade to Map.
 * @param m Operations mapping NAME -> FUNCTION
 */
protected class LibraryImpl(m: FuncMap) extends Library {
  /**
   * Resolve operation name to a function
   * @param op Operation name
   * @return Operation function for calculation
   */
  override def resolve(op: String): Func = {
    if (m.contains(op)) m(op)
    else throw UnrecognizedOperationException(op)
  }
}

/**
 * Library manager factory.
 */
object Library {
  /** APPLY operation, reserved word */
  val ApplyOperation = "APPLY"

  /**
   * Identity functions to be used for initialization of computations
   * @param x Apply operand value
   * @return Input value
   */
  def Id(x: Int): Int = x

  /**
   * Verify that operations definitions are all unique or fail
   * @param list List of function definitions
   * @return Input list if all definitions are unique or throws exception
   * @throws AmbiguousDefinitionsFoundException If not unique definitions
   */
  private def verifyDistinct(list: FuncList): FuncList = {
    // collect list of unique definitions and list of ambiguous definitions
    val splitValues = ((List[String](), List[String]()) /: list)((acc, funcDef) =>
    // Function is not APPLY and was not previously processed
      if ((funcDef._1 != ApplyOperation) && !acc._1.contains(funcDef._1)) (funcDef._1 :: acc._1, acc._2)
      // otherwise include in ambiguous definitions list
      else (acc._1, funcDef._1 :: acc._2))
    if (!splitValues._2.isEmpty) throw AmbiguousDefinitionsFoundException(splitValues._2) else list
  }

  /**
   * Construct library manager out of list of operation libraries
   * @param libs Operation libraries
   * @return Library manager
   * @throws AmbiguousDefinitionsFoundException If not unique definitions
   */
  def apply(libs: List[OperationLibrary]) = {
    val list = libs.flatMap(ol => ol.mappings)
    new LibraryImpl(verifyDistinct(list).toMap)
  }

  /**
   * Construct library manager out of operation library
   * @param lib Operation library
   * @return Library manager
   * @throws AmbiguousDefinitionsFoundException If not unique definitions
   */
  def apply(lib: OperationLibrary) = {
    new LibraryImpl(verifyDistinct(lib.mappings).toMap)
  }

  /**
   * Construct library manager out of list of operation mapping
   * @param m Map of NAME -> FUNCTION
   * @return Library manager
   */
  def apply(m: FuncMap) = {
    new LibraryImpl(m)
  }
}
