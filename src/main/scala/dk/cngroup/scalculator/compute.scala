package dk.cngroup.scalculator

import dk.cngroup.scalculator.Types._

/**
 * Simple stateless computer to evaluate the program.
 */
object compute {

  /**
   * Calculator recursive immutable implementation.
   * @param func Computation function constructed so far. Should be ID function in initial call.
   * @param instrIt Proram - instruction iterator
   * @param acc Accumulator to gather computation results
   * @param library Library of operations
   * @return List of computed results
   */
  private def calculateImpl(func: (Int) => Int, instrIt: InstrIterator, acc: List[Int], library: Library): List[Int] = {
    if (instrIt.hasNext) {
      val instr = instrIt.next()
      if (instr._1 == Library.ApplyOperation) calculateImpl(func, instrIt, func(instr._2) :: acc, library)
      else calculateImpl(library.resolve(instr._1)(func, instr._2), instrIt, acc, library)
    }
    else acc
  }

  /**
   * Factory method to access computation object
   * @param reader Reader to iterate through instructions
   * @param library Library of operations
   * @return List of computed results
   */
  def apply(reader: Reader, library: Library) = {
    calculateImpl(Library.Id, reader.instructions, List[Int](), library).reverse
  }
}
