package dk.cngroup.scalculator

import dk.cngroup.scalculator.Types.InstrIterator
import scala.io.Source

/**
 * Exception to be throws in case of wrong computation input.
 * @param instruction Instruction input line
 * @param cause Original cause, may be null
 */
case class InvalidInputException(instruction: String, cause: Exception) extends Exception("Invalid input line: " + instruction, cause) {
  /**
   * Exception to be throws in case of wrong computation input.
   * @param instruction Instruction input line
   */
  def this(instruction: String) = this(instruction, null)
}

/**
 * Reader interface.
 */
trait Reader {
  /** Instruction iterator */
  def instructions: InstrIterator

  /**
   * Parse instruction line into instruction
   * @param line Input line
   * @return Parsed instruction
   */
  protected def parseLine(line: String): (String, Int) = {
    // trim and split by whitespaces
    val parts = line.trim.split("\\s")
    // each instruction has to have 2 parts
    if (parts.length != 2) throw new InvalidInputException(line)
    else try {
      // first part in OPERATION, second is OPERAND
      (parts(0), parts(1).toInt)
    } catch {
      case e: NumberFormatException => throw new InvalidInputException(line, e)
    }
  }
}

/**
 * File based instruction reader.
 * @param file Path to file to read from
 */
protected class FileReader(file: String) extends Reader {
  /** Instruction iterator */
  override def instructions: InstrIterator = {
    Source.fromFile(file).getLines().map((line: String) => parseLine(line))
  }
}

/**
 * String based instruction reader
 * @param lines Instruction input lines
 */
protected class StringReader(lines: List[String]) extends Reader {
  /** Instruction iterator */
  override def instructions: InstrIterator = {
    lines.iterator.map((line: String) => parseLine(line))
  }
}

/**
 * Reader factory.
 */
object Reader {
  /**
   * Create reader from file.
   * @param file Path to file to read from
   * @return File reader
   */
  def apply(file: String): Reader = {
    new FileReader(file)
  }

  /**
   * Create reader from string list.
   * @param prog Instruction input lines
   * @return String reader
   */
  def apply(prog: List[String]): Reader = {
    new StringReader(prog)
  }
}
