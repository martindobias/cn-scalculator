package dk.cngroup.scalculator.test

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest._
import dk.cngroup.scalculator.{UnrecognizedOperationException, AmbiguousDefinitionsFoundException, Library}
import dk.cngroup.scalculator.library.BasicOperations

@RunWith(classOf[JUnitRunner])
class LibrarySpec extends FlatSpec with ShouldMatchers {

  "A library" should "resolve operation by name" in {
    val lib = Library(Map("getOp" -> ((f: (Int) => Int, v: Int) => (x: Int) => v), "getBase" -> ((f: (Int) => Int, v: Int) => (x: Int) => x)))
    lib.resolve("getOp")(Library.Id, 2)(6) should be(2)
    lib.resolve("getBase")(Library.Id, 2)(6) should be(6)
    intercept[UnrecognizedOperationException] {
      lib.resolve("unknown")
    }
  }

  it should "find all basic operations" in {
    val lib = Library(BasicOperations)
    lib.resolve("ADD")(Library.Id, 2)(6) should be(8)
    lib.resolve("SUB")(Library.Id, 2)(6) should be(4)
    lib.resolve("MUL")(Library.Id, 2)(6) should be(12)
    lib.resolve("DIV")(Library.Id, 2)(6) should be(3)
  }

  it should "not allow ambiguous definitions" in {
    intercept[AmbiguousDefinitionsFoundException] {
      Library(List(BasicOperations, BasicOperations))
    }
  }
}
