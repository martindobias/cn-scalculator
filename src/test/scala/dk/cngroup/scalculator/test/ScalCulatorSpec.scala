package dk.cngroup.scalculator.test

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import dk.cngroup.scalculator._
import dk.cngroup.scalculator.library.BasicOperations
import dk.cngroup.scalculator.InvalidInputException

@RunWith(classOf[JUnitRunner])
class ScalCulatorSpec extends FeatureSpec with GivenWhenThen with ShouldMatchers {

  info("As a user")
  info("I want to be able to compute value of a script")
  info("so that the result is printed on output")
  info("or else exception is thrown")

  feature("Calculator") {
    scenario("User selects file for calculation") {
      Given("Basic library is available")
      val lib = Library(BasicOperations)

      When("User selects valid file")
      val r = Reader("src/test/resources/test1.txt")

      Then("Result shall be 2")
      compute(r, lib) should be(List(2))
    }

    scenario("User selects file with invalid input for calculation") {
      Given("Basic library is available")
      val lib = Library(BasicOperations)

      When("User selects file with wrong instruction syntax")
      val r2 = Reader("src/test/resources/test2.txt")

      Then("Exception is thrown during calculation")
      intercept[InvalidInputException] {
        compute(r2, lib)
      }

      When("User selects file with wrong operand definition")
      val r3 = Reader("src/test/resources/test3.txt")

      Then("Exception is thrown during calculation")
      intercept[InvalidInputException] {
        compute(r3, lib)
      }

      When("User selects file with unknown operation")
      val r4 = Reader("src/test/resources/test4.txt")

      Then("Exception is thrown during calculation")
      intercept[UnrecognizedOperationException] {
        compute(r4, lib)
      }
    }
  }

}
