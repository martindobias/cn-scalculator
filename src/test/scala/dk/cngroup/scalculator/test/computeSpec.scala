package dk.cngroup.scalculator.test

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.scalamock.scalatest.MockFactory
import dk.cngroup.scalculator.{compute, Library, Reader}
import dk.cngroup.scalculator.library.BasicOperations

@RunWith(classOf[JUnitRunner])
class computeSpec extends FlatSpec with ShouldMatchers with MockFactory {

  "A Computer" should "evaluate simple script" in {
    val mockReader = mock[Reader]
    val mockLibrary = mock[Library]

    val prog = ("ADD", 2) ::("MUL", 2) ::("SUB", 0) ::("APPLY", 2) ::("APPLY", 3) :: Nil
    (mockReader.instructions _) expects() returning prog.iterator
    (mockLibrary.resolve _) expects ("ADD") returning BasicOperations.add
    (mockLibrary.resolve _) expects ("MUL") returning BasicOperations.multiply
    (mockLibrary.resolve _) expects ("SUB") returning BasicOperations.subtract

    val result = compute(mockReader, mockLibrary)
    result should be(8 :: 10 :: Nil)
  }

}
