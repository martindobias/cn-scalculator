package dk.cngroup.scalculator.test

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import dk.cngroup.scalculator.{InvalidInputException, Reader}

@RunWith(classOf[JUnitRunner])
class ReaderSpec extends FlatSpec with ShouldMatchers {

  "A Reader" should "load instructions from valid file and fail for the rest" in {
    Reader("src/test/resources/test1.txt").instructions.length should be(2)

    Range(2, 4).map((i: Int) => "src/test/resources/test" + i + ".txt").foreach((file: String) =>
      intercept[InvalidInputException] {
        Reader(file).instructions.length
      })
  }

  it should "load instructions from memory" in {
    val prog = "ADD 2" :: "MUL 2" :: "SUB 0" :: "APPLY 2" :: Nil
    Reader(prog).instructions.length should be(4)
  }

}
